package pa5;

import java.util.Date;
import java.util.TimerTask;
import java.io.*;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

import sun.audio.*;

/**
 * Clock object that runs and update the time in UI
 * @author Parinvut Rochanavedya
 * @version 05-04-2015
 */
public class Clock {
	/**Time to dislay*/
	private Date time = new Date();
	
	/**alarm time*/
	public int alarm_hr, alarm_min, alarm_sec;
	
	/**States code*/
	public final int DISPLAY_STATE = 0;
	public final int SET_ALARM_STATE = 1;
	public final int RINGING_ALARM_STATE = 2;
	
	/**State handler*/
	private ClockState state;
	public DigitalClockUI ui;  
	private final ClockState displayState = new DisplayState(this);
	private final ClockState setAlarmState = new SetAlarmState(this);
	private final ClockState ringingState = new RingingState(this);
	
	/**Sound Control Variables*/
	private final File sndFile = new File("res//alarm.wav");
	private AudioInputStream audioInput;
	private Clip clip;
	/**
	 * Constructor
	 */
	public Clock(DigitalClockUI ui){
		setState(DISPLAY_STATE);
		this.ui = ui;
		alarm_hr = 0;
		alarm_min = 0;
		alarm_sec = 0;
		
		
	}
	/**
	 * Set alarm time
	 */
	public void setAlarmTime(int hr, int min, int sec){
		alarm_hr = hr;
		alarm_min = min;
		alarm_sec = sec;
	}
	/**
	 * Update the (display) time in UI 
	 */
	public void updateTime(){
		state.updateDisplayTime();
	}
	/**
	 * Set the clock's state
	 * @param state: the index of state type
	 */
	public void setState(int state){
		switch(state){
		case DISPLAY_STATE:
			this.state = displayState;
			break;
		case SET_ALARM_STATE:
			this.state = setAlarmState;
			break;
		case RINGING_ALARM_STATE:
			this.state = ringingState;
			break;
		}
	}
	/**
	 * Set button action handler for any states
	 */
	public void doSet(){
		state.doSetAction();
	}
	/**
	 * Plus button action handler for any states
	 */
	public void doPlus(){
		state.doPlusAction();
	}
	/**
	 * Minus button action handler for any states
	 */
	public void doMinus(){
		state.doMinusAction();
	}
	/**
	 * Play alarm sound
	 */
	public void alarm(){
		try{
			audioInput = AudioSystem.getAudioInputStream(sndFile);
			clip = AudioSystem.getClip();
			clip.open(audioInput);
		} catch (Exception e){
			System.err.println("Cannot load sound");
			return;
		}	
		clip.start();
		clip.loop(Clip.LOOP_CONTINUOUSLY);
	}
	/**
	 * Alarm sound clip
	 */
	public Clip getAlarm(){
		return clip;
	}
}
