package pa5;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Date;

/**
 * The display state, the normal state of the clock
 * @author Parinvut Rochanavedya
 * @version 05-04-2015
 */
public class DisplayState implements ClockState{
	/**Target Clock*/
	private Clock clock;
	/**Current System Time*/
	private Date time = new Date();
	/**
	 * Constructor
	 */
	public DisplayState(Clock clock){
		this.clock = clock;
	}
	
	@Override
	public void updateDisplayTime() {
		time.setTime( System.currentTimeMillis() );
		int hours = time.getHours();
		int minutes = time.getMinutes();
		int secs = time.getSeconds();
		
		if(hours == clock.alarm_hr &&
				minutes == clock.alarm_min &&
				secs == clock.alarm_sec){
				
					clock.setState(clock.RINGING_ALARM_STATE);
					
					return;
			
			}
		
		clock.ui.time_hr.setText(hours+"");
		clock.ui.time_min.setText(minutes+"");
		clock.ui.time_sec.setText(secs+"");
		
		this.clock.ui.setTimeDisplayNormalColor();
		
		if(this.clock.getAlarm().isRunning()){
			this.clock.getAlarm().stop();
		}
		
	}

	@Override
	public void doSetAction() {
		clock.setState(clock.SET_ALARM_STATE);
	}

	@Override
	public void doPlusAction() {
		int hours = clock.alarm_hr;
		int minutes = clock.alarm_min;
		int secs = clock.alarm_sec;
		
		this.clock.ui.setTimeDisplayAlarmColor();
		
		clock.ui.time_hr.setText(hours+"");
		clock.ui.time_min.setText(minutes+"");
		clock.ui.time_sec.setText(secs+"");
	}

	@Override
	public void doMinusAction() {
		// do nothing
	}
	
}
