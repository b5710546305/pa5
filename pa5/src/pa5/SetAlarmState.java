package pa5;

import java.awt.Color;
import java.awt.event.KeyEvent;

/**
 * The set alarm state that set the alarm time
 * sub states : hr -> min -> sec
 * @author Parinvut Rochanavedya
 * @version 05-04-2015
 */
public class SetAlarmState implements ClockState{
	/**Target Clock*/
	private Clock clock;
	/**Sub states*/
	private final int SET_HOUR_STATE = 0;
	private final int SET_MIN_STATE = 1;
	private final int SET_SECOND_STATE = 2;
	
	private int timeState;
	
	/**
	 * Constructor
	 */
	public SetAlarmState(Clock clock){
		this.clock = clock;
		timeState = SET_HOUR_STATE;
	}
	
	@Override
	public void updateDisplayTime() {
		int hours = clock.alarm_hr;
		int minutes = clock.alarm_min;
		int secs = clock.alarm_sec;
		
		switch(timeState){
		case SET_HOUR_STATE:
			clock.ui.time_hr.setForeground(Color.YELLOW);
			break;
		case SET_MIN_STATE:
			clock.ui.time_hr.setForeground(Color.CYAN);
			clock.ui.time_min.setForeground(Color.YELLOW);
			break;
		case SET_SECOND_STATE:
			clock.ui.time_hr.setForeground(Color.CYAN);
			clock.ui.time_min.setForeground(Color.CYAN);
			clock.ui.time_sec.setForeground(Color.YELLOW);
			break;
		}
		
		clock.ui.time_hr.setText(hours+"");
		clock.ui.time_min.setText(minutes+"");
		clock.ui.time_sec.setText(secs+"");
	}

	@Override
	public void doSetAction() {
		switch(timeState){
		case SET_HOUR_STATE:
			timeState = SET_MIN_STATE;
			break;
		case SET_MIN_STATE:
			timeState = SET_SECOND_STATE;
			break;
		case SET_SECOND_STATE:
			timeState = SET_HOUR_STATE;
			clock.setState(clock.DISPLAY_STATE);
			break;
		}
	}
	
	@Override
	public void doPlusAction() {
		switch(timeState){
		case SET_HOUR_STATE:
			clock.alarm_hr++;
			if(clock.alarm_hr > 23){
				clock.alarm_hr = 0;
				}
			break;
		case SET_MIN_STATE:
			clock.alarm_min++;
			if(clock.alarm_min > 59){
				clock.alarm_min = 0;
				}
			break;
		case SET_SECOND_STATE:
			clock.alarm_sec++;
			if(clock.alarm_sec > 59){
				clock.alarm_sec = 0;
				}
			break;
		}
	}

	@Override
	public void doMinusAction() {
		switch(timeState){
		case SET_HOUR_STATE:
			clock.alarm_hr--;
			if(clock.alarm_hr < 0){
				clock.alarm_hr = 23;
				}
			break;
		case SET_MIN_STATE:
			clock.alarm_min--;
			if(clock.alarm_min < 0){
				clock.alarm_min = 59;
				}
			break;
		case SET_SECOND_STATE:
			clock.alarm_sec--;
			if(clock.alarm_sec < 0){
				clock.alarm_sec = 59;
				}
			break;
		}
	}


}
