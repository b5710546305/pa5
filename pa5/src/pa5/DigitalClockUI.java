package pa5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
/**
 * The main application for this project
 * Digital Clock alarm system using state pattern
 * @author Parinvut Rochanavedya
 * @version 05-04-2015
 */
public class DigitalClockUI extends JFrame {
	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Components
	 */
	private JPanel contentPane;
	public JLabel time_hr;
	public JLabel time_colon_1;
	public JLabel time_min;
	public JLabel time_colon_2;
	public JLabel time_sec;
	public JButton btnSet, btnPlus, btnMinus;
	final static long INTERVAL = 500; // millisec

	private static Clock clock;
	private static TimerTask clocktask;
	private static Timer timer;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DigitalClockUI frame = new DigitalClockUI();
					frame.setVisible(true);
					
					//This is how the clock works
					clocktask = new ClockTask( clock );
					timer = new Timer();
					
					long delay = 1000 - System.currentTimeMillis()%1000;
					// you can also use timer.schedule here
					timer.scheduleAtFixedRate( clocktask, delay, INTERVAL );
					

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DigitalClockUI() {
		setResizable(false);
		setBackground(Color.DARK_GRAY);
		setTitle("Digital Clock");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 322, 201);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		/**
		 * Clock engine
		 */
		clock = new Clock(this);
		
		time_hr = new JLabel("0");
		time_hr.setForeground(Color.CYAN);
		time_hr.setFont(new Font("DilleniaUPC", Font.PLAIN, 63));
		time_hr.setBounds(44, 39, 50, 50);
		contentPane.add(time_hr);
		
		time_min = new JLabel("0");
		time_min.setForeground(Color.CYAN);
		time_min.setFont(new Font("DilleniaUPC", Font.PLAIN, 63));
		time_min.setBounds(142, 39, 50, 50);
		contentPane.add(time_min);
		
		time_sec = new JLabel("0");
		time_sec.setForeground(Color.CYAN);
		time_sec.setFont(new Font("DilleniaUPC", Font.PLAIN, 63));
		time_sec.setBounds(243, 39, 50, 50);
		contentPane.add(time_sec);
		
		/**Set Alarm Button*/
		btnSet = new JButton("Set");
		btnSet.setFont(new Font("Trebuchet MS", Font.PLAIN, 11));
		btnSet.setBackground(Color.CYAN);
		btnSet.setBounds(10, 121, 89, 23);
		contentPane.add(btnSet);
		
		btnPlus = new JButton("+");
		btnPlus.setBackground(Color.CYAN);
		btnPlus.setBounds(112, 121, 89, 23);
		contentPane.add(btnPlus);
		
		btnMinus = new JButton("-");
		btnMinus.setBackground(Color.CYAN);
		btnMinus.setBounds(215, 121, 89, 23);
		contentPane.add(btnMinus);
		
		time_colon_1 = new JLabel(":");
		time_colon_1.setForeground(Color.CYAN);
		time_colon_1.setFont(new Font("DilleniaUPC", Font.PLAIN, 63));
		time_colon_1.setBounds(100, 39, 19, 50);
		contentPane.add(time_colon_1);
		
		time_colon_2 = new JLabel(":");
		time_colon_2.setForeground(Color.CYAN);
		time_colon_2.setFont(new Font("DilleniaUPC", Font.PLAIN, 63));
		time_colon_2.setBounds(204, 39, 19, 50);
		contentPane.add(time_colon_2);
		
		btnSet.addActionListener(new SetAction());
		btnPlus.addActionListener(new PlusAction());
		btnMinus.addActionListener(new MinusAction());
		
	}
	/**
	 * Set time display colors
	 */
	public void setTimeDisplayAlarmColor(){
		time_hr.setForeground(Color.YELLOW);
		time_min.setForeground(Color.YELLOW);
		time_sec.setForeground(Color.YELLOW);
		time_colon_1.setForeground(Color.YELLOW);
		time_colon_2.setForeground(Color.YELLOW);
	}
	public void setTimeDisplayNormalColor(){
		time_hr.setForeground(Color.CYAN);
		time_min.setForeground(Color.CYAN);
		time_sec.setForeground(Color.CYAN);
		time_colon_1.setForeground(Color.CYAN);
		time_colon_2.setForeground(Color.CYAN);
	}
	/**
	 * Set button action
	 */
	private class SetAction implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			clock.doSet();
		}
	}
	/**
	 * Plus button action
	 */
	private class PlusAction implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			clock.doPlus();
		}
	}
	/**
	 * Minus button action
	 */
	private class MinusAction implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			clock.doMinus();
		}
	}
}