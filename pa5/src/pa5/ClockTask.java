package pa5;

import java.util.TimerTask;

/**
 * Clock Task updates the clock
 * @author Parinvut Rochanavedya
 * @version 05-04-2015
 */
public class ClockTask extends TimerTask{
	/**Target Clock*/
	private Clock clock;
	/**
	 * Constructor
	 */
	public ClockTask(Clock targetClock){
		clock = targetClock;
	}
	/**
	 * Run (from Runnable)
	 */
	public void run() {
		clock.updateTime();
	}

}
