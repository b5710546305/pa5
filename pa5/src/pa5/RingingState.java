package pa5;

import java.awt.event.KeyEvent;
import java.util.Date;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * The ringing state of the clock, the alarm activates and play sound
 * @author Parinvut Rochanavedya
 * @version 05-04-2015
 */
public class RingingState implements ClockState{
	/**Target Clock*/
	private Clock clock;
	/**Current System Time*/
	private Date time = new Date();
	/**
	 * Constructor
	 */
	public RingingState(Clock clock){
		this.clock = clock;
	}
	
	@Override
	public void updateDisplayTime() {
		time.setTime( System.currentTimeMillis() );
		int hours = time.getHours();
		int minutes = time.getMinutes();
		int secs = time.getSeconds();
		
		this.clock.ui.setTimeDisplayAlarmColor();
		
		clock.ui.time_hr.setText(hours+"");
		clock.ui.time_min.setText(minutes+"");
		clock.ui.time_sec.setText(secs+"");
		
		clock.alarm();
		
	}

	@Override
	public void doSetAction() {
		clock.setState(clock.DISPLAY_STATE);
	}
	
	@Override
	public void doPlusAction() {
		clock.setState(clock.DISPLAY_STATE);
	}

	@Override
	public void doMinusAction() {
		clock.setState(clock.DISPLAY_STATE);
	}


}