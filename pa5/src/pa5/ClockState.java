package pa5;

import java.awt.event.KeyEvent;

/**
 * An interface for O-O State of every clock states
 * @author Parinvut Rochanavedya
 * @version 05-04-2015
 */
public interface ClockState {
	/**
	 * Display the time
	 */
	void updateDisplayTime();
	/**
	 * Set button action
	 */
	void doSetAction();
	/**
	 * Plus button action
	 */
	void doPlusAction();
	/**
	 * Minus button action
	 */
	void doMinusAction();
}
